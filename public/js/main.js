function processing(text) {
    $("#console").append("<span style='color:yellow'>" + text + "</span><br>");
}
function normal(text) {
    $("#console").append( text + "<br>");
}
function success(text) {
    $("#console").append("<span style='color:#66ffb3'>" + text + "</span><br>");
}
function error(text) {
    $("#console").append("<span style='color:#ff5050'>" + text + "</span><br>");
}

$(function() { 
    $("#upload").click(function(ev) {
        
        var uploadForm = document.createElement('form');
        var fileInput = uploadForm.appendChild(document.createElement('input'));
        fileInput.type = 'file';
        fileInput.accept = ".csv";
        fileInput.name = 'dataFile';
        fileInput.multiple = false;
        fileInput.onchange = function(e) {
            processing("Feeding the duck overlords, please wait..")
            const file = e.target.files[0];//our csv file
            let formData = new FormData();
            formData.append("dataFile", file);
            $.ajax({
                url: '/api/upload',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                type: 'POST', 
                success: function(data){
                    success("Lord has been fed! Here is what he says:");
                    normal(data);
                }
            }).fail(function( jqXHR, textStatus, errorThrown ) {
                console.log(jqXHR, textStatus, errorThrown);
                error("The duck overlord is unsatisfied! Check chrome console for more information.")
            });
        };
        fileInput.click();
    });
});